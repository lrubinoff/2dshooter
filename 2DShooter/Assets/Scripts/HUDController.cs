﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {

	[SerializeField]
	Text points = null;
	[SerializeField]
	Text health = null;

	[SerializeField]
	GameObject player = null;
	[SerializeField]
	Text gameover = null;

	[SerializeField]
	Button newGameButton = null;


	// Use this for initialization
	void Start () {
		points.text = "Points: 0";
		health.text = "Health: 100";
		Player.Instance.hud = this;
		NewGame ();
	}

	public void UpdatePoints(){

		points.text = "Points: " + Player.Instance.Points;

	}

	public void UpdateHealth(){
		health.text = "Health: " + Player.Instance.Health;
	}

	public void GameOver(){
		points.gameObject.SetActive (false);
		health.gameObject.SetActive (false);
		player.SetActive (false);

		gameover.gameObject.SetActive (true);
		newGameButton.gameObject.SetActive (true);
	}

	public void NewGame(){
		points.gameObject.SetActive (true);
		health.gameObject.SetActive (true);
		player.SetActive (true);

		gameover.gameObject.SetActive (false);
		newGameButton.gameObject.SetActive (false);
		Player.Instance.Health = 100;
		Player.Instance.Points = 0;

	}
	// Update is called once per frame
	void Update () {
	
	}
}
