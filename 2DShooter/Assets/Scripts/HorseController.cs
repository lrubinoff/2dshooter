﻿using UnityEngine;
using System.Collections;

public class HorseController : MonoBehaviour {

	[SerializeField]
	private float speed;

	private Transform _transform;
	private Vector2 _currentPosition;
	private float _playerInput;
	private float _playerInput1;

	// Use this for initialization
	void Start () {

		_transform = gameObject.GetComponent<Transform>();
		_currentPosition = _transform.position;
	}

	// Update is called once per frame
	void FixedUpdate () {

		_playerInput = Input.GetAxis ("Horizontal");
		_currentPosition = _transform.position;

		_playerInput1 = Input.GetAxis ("Vertical");
		//move right
		if (_playerInput > 0) {
			_currentPosition += new Vector2 (speed, 0);
		}

		//move left
		if (_playerInput < 0) {
			_currentPosition -= new Vector2 (speed, 0);
		}

		//move up
		if (_playerInput1 > 0) {
			_currentPosition += new Vector2 (0, speed);
		}

		//move down
		if (_playerInput1 < 0) {
			_currentPosition -= new Vector2 (0, speed);
		}

		//fix bounds
		checkBounds ();
		_transform.position = _currentPosition;
	}

	private void checkBounds(){

		if (_currentPosition.x < -3.3f) {
			_currentPosition.x = -3.3f;
		}

		if (_currentPosition.x > 3.3f) {
			_currentPosition.x = 3.3f;
		}

		if (_currentPosition.y < -0.97f) {
			_currentPosition.y = -0.97f;
		}

		if (_currentPosition.y > 0) {
			_currentPosition.y = 0f;
		}
	}

}
