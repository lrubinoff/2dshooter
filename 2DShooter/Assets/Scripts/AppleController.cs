﻿using UnityEngine;
using System.Collections;

public class AppleController : MonoBehaviour {

	[SerializeField]
	private float speed;

	private Transform _transform;
	private Vector2 _currentPosition;

	private float minx = -15f;
	private float maxx = 3.5f;

	private float miny = -1.14f;
	private float maxy = -0.13f;

	// Use this for initialization
	void Start () {
		_transform = gameObject.GetComponent<Transform>();
		_currentPosition = _transform.position;
		Reset ();

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		_currentPosition = _transform.position;

		_currentPosition += new Vector2 (speed, 0);
		_transform.position = _currentPosition;

		if (_currentPosition.x >= -4.1f) {
			Reset ();
		}
	}

	private void Reset(){

		float xpos = Random.Range (minx, maxx);
		float ypos = Random.Range (miny, maxy);

		_currentPosition = new Vector2 (xpos, ypos);
		_transform.position = _currentPosition;
		
	}

}
